### golang http客户端工具

#### 支持功能:
1. 重试机制
2. 请求唯一标识
3. 通过回调完成结果处理
4. 自定义header头，同时处理并发时header头问题
5. 支持post/get 请求

#### 下载方式
```
go get -u gitee.com/tym_hmm/go-httpclient
```

#### 使用方式
```cgo
/**
自定义超时时间请求
@param timeOut int 超时时间
**/

[使用方法]
client := HttpClient.NewHttpClient()
//第一个参数为超时时间(精到秒)
client.SetOutTime(5, 5)
//注意 使用常用header 尽量保持首字符大写 如：Content-Type
headerMap := map[string]string{
     "Content-Type": "application/x-www-form-urlencoded",
}
httpClient.AddHeader(headerMap)
//设置传参
dataParam := make(map[string]string)
dataParam["notifyIdentifier"] = "21321"
dataParam["userIdentifier"] = "21321"
dataParam["extraInfo"] = "21321"
dataParam["orderId"] = "21321"

httpClient.Init(url, dataParam)
httpClient.SetSuccessListener(func(identifier string, response *Request.HttpResponse) {
    //content := response.Content
})
httpClient.SetErrorListener(func(s string, httpError *Request.HttpError) {
    
})
//发送post请求
httpClient.HttpPostReTryTimeOut()
//发送get请求
client.HttpGetReTryTimeOut()
```
#### 接口说明
* 设置cookie
```cgo
//设置cookie
SetCookie(cookie []*http.Cookie)

```
* 设置超时时间
```cgo
SetOutTime(time int, typeTime TimeOutType) 
```

|名称|类型|说明|
|---|---|---|
|time|int| 时间值，根据typeTime 指定秒还是毫秒|
|typeTime |TimeOutType | 时间类型 <br>TIME_OUT_TYPE_SECOND（秒）<br>TIME_OUT_TYPE_MILLISECOND (毫秒)|

* 设置最大重试次数(默认为5次)
```cgo
SetMaxRetryTime(retryTime int)
```


* 错误回调
```cgo
SetErrorListener(fn FnError)
```

* 成功回调
```cgo
 SetSuccessListener(fn FnSuccess)
```


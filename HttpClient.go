package HttpClient

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	defaultHeader = map[string]string{
		"Content-Type": "charset=utf-8",
		//"User-Agent":   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
	}
	DEFAULT_NODE_ID int64 = 1
)

/*
*
客户端访问请求
@param url string 发送地址
@param data interface{} 发送数据
@param contentType string 数据类型
@param timeout int 超时时间(秒)
*/
type HttpResponse struct {
	Resp         *http.Response
	StatusCode   int    //请求状态
	Content      string //内容
	ContentBytes []byte //内容数组
	Cookie       []*http.Cookie
	Header       http.Header
	TryTime      int
}

type HttpError struct {
	Code    int
	Error   error
	TryTime int
}

type FnError func(string, *HttpError)
type FnSuccess func(string, *HttpResponse)

type TimeOutType int

const (
	HTTP_ERROR_TIME_OUT = 122 //超时
	HTTP_SUCCESS        = 200 //正常
	HTTP_ERROR          = 101 //错误

	HTTP_TIME_OUT     = 5 //默认超时时间(秒)
	HTTP_MAX_TRY_TIME = 5 //默认最大重试次数

	TIME_OUT_TYPE_SECOND      TimeOutType = 11
	TIME_OUT_TYPE_MILLISECOND TimeOutType = 12
)

type httpClient struct {
	nodeId      int64 //节点id
	maxTryTime  int   //请求最大次数
	tryTime     int   //当前尝试次数
	timeOut     int   //超时时间(秒)
	typeOutTime TimeOutType

	ctx context.Context // 上下文

	identifier string            //请求标识 每次处理请求都唯一
	url        string            //请求地址
	data       interface{}       //请求数据
	header     map[string]string //请求header
	cookie     []*http.Cookie

	errorFun   FnError   //错误回调
	successFun FnSuccess //成功回调

	headerReadLock *sync.RWMutex
}

func getDefaultHeader() map[string]string {
	outHeader := make(map[string]string)

	for k, v := range defaultHeader {
		outHeader[k] = v
	}
	return outHeader
}

/*
*
防止并发时对全局map  同时写及读
*/
func (h *httpClient) GetHeader() map[string]string {
	return h.header
}

func (h *httpClient) AddHeader(setheader map[string]string) {
	h.headerReadLock.Lock()
	for k, v := range setheader {
		h.header[k] = v
	}
	h.headerReadLock.Unlock()
}

func NewHttpClientTimeOut(timeOut int, nodeId int64) *httpClient {
	return &httpClient{ctx: nil, nodeId: nodeId, maxTryTime: HTTP_MAX_TRY_TIME, tryTime: 1, timeOut: timeOut, headerReadLock: new(sync.RWMutex), header: getDefaultHeader()}
}

func NewHttpClientTimeOutCtx(ctx context.Context, timeOut int, nodeId int64) *httpClient {
	return &httpClient{ctx: ctx, nodeId: nodeId, maxTryTime: HTTP_MAX_TRY_TIME, tryTime: 1, timeOut: timeOut, headerReadLock: new(sync.RWMutex), header: getDefaultHeader()}
}

/*
*
默认请求
*/
func NewHttpClient() *httpClient {
	return NewHttpClientTimeOut(HTTP_TIME_OUT, DEFAULT_NODE_ID)
	//return &httpClient{maxTryTime: HTTP_MAX_TRY_TIME, tryTime: 1, timeOut: HTTP_TIME_OUT, headerReadLock: new(sync.RWMutex), header: getDefaultHeader()}
}

func NewHttpClientWithCtx(ctx context.Context) *httpClient {
	return NewHttpClientTimeOutCtx(ctx, HTTP_TIME_OUT, DEFAULT_NODE_ID)
}

func NewHttpClientNodeId(nodeId int64) *httpClient {
	return NewHttpClientTimeOut(HTTP_TIME_OUT, nodeId)
}

func NewHttpClientNodeIdWithCtx(ctx context.Context, nodeId int64) *httpClient {
	return NewHttpClientTimeOutCtx(ctx, HTTP_TIME_OUT, nodeId)
}

/*
*
初始化请求
*/
func (h *httpClient) Init(url string, data interface{}) {
	identifier, err := getUniqueNum(h.nodeId)
	if err != nil {
		log("生成标识错误")
	}
	h.identifier = identifier
	h.url = url
	h.data = data
	//h.header = header
}

func (h *httpClient) SetCookie(cookie []*http.Cookie) {
	h.cookie = cookie
}
func (h *httpClient) SetOutTime(t int, typeTime TimeOutType) {
	h.timeOut = t
	h.typeOutTime = typeTime
}

func (h *httpClient) SetMaxRetryTime(retryTime int) {
	h.maxTryTime = retryTime
}

func (h *httpClient) SetErrorListener(fn FnError) {
	h.errorFun = fn
}
func (h *httpClient) SetSuccessListener(fn FnSuccess) {
	h.successFun = fn
}

/*
*
http 发送带超时回调用get请求
@param httpUrl string 请求地址
@param data interface 请求参数
@param header map[string]interface 头部信息
@param timeout int 超时时间
@param tryTime int 重式次数
@param maxTryTime int 最大重试次数
*/
func (h *httpClient) HttpPostReTryTimeOut() {
	if len(strings.TrimSpace(h.url)) == 0 {
		errsd := &HttpError{
			Code:  HTTP_ERROR,
			Error: errors.New("request url error empty"),
		}
		if h.errorFun != nil {
			h.errorFun(h.identifier, errsd)
		} else {
			errsd.Error = errors.New("request url error empty")
			h.errorFun(h.identifier, errsd)
		}
		return
	}
	//先发送一次请求
	var httpResponse *HttpResponse
	var httpReponseError *HttpError
	if h.ctx == nil {
		httpResponse, httpReponseError = HttpPost(h, h.url, h.data, h.header, h.cookie, h.timeOut)
	} else {
		httpResponse, httpReponseError = HttpPostWithCtx(h.ctx, h, h.url, h.data, h.header, h.cookie, h.timeOut)
	}

	requestMsg := fmt.Sprintf("[info]发起请求-%s:[%s],method:[%s] 当前次数:[%d], 参数:[%+v], 头信息:[%+v]", h.identifier, h.url, "POST", h.tryTime, h.data, h.header)
	log(requestMsg)
	//如果请求错误
	if httpReponseError != nil {
		httpReponseError.TryTime = h.tryTime
		//请求超时
		switch httpReponseError.Code {
		case HTTP_ERROR_TIME_OUT: //请求超时
			if h.tryTime >= h.maxTryTime { //超过次数
				httpReponseError.TryTime = h.tryTime
				requestMsg := fmt.Sprintf("[error]请求超时-%s:[%s], 当前次数:[%d], 参数:[%+v], 头信息:[%+v]", h.identifier, h.url, h.tryTime, h.data, h.header)
				log(requestMsg)
				if h.errorFun != nil {
					h.errorFun(h.identifier, httpReponseError)
				}
				return
			} else {
				h.tryTime++
				h.HttpPostReTryTimeOut()
				return
			}
		default: //通用错误
			requestMsg := fmt.Sprintf("[error]请求错误-%s:[%s], 当前次数:[%d], 参数:[%+v], 头信息:[%+v], error:[%+v]", h.identifier, h.url, h.tryTime, h.data, h.header, httpReponseError)
			log(requestMsg)
			h.errorFun(h.identifier, httpReponseError)
			return
		}
	}
	httpResponse.TryTime = h.tryTime
	if h.successFun != nil {
		h.successFun(h.identifier, httpResponse)
	}
}

/*
*
发送带超时回调用get请求
*/
func (h *httpClient) HttpGetReTryTimeOut() {
	if len(strings.TrimSpace(h.url)) == 0 {
		errsd := &HttpError{
			Code:  HTTP_ERROR,
			Error: errors.New("request url error empty"),
		}
		if h.errorFun != nil {
			h.errorFun(h.identifier, errsd)
		} else {
			fmt.Println("request url error empty")
		}
		return
	}
	//先发送一次请求
	//httpResponse, httpReponseError := HttpGet(h, h.url, h.data, h.header, h.cookie, h.timeOut)

	var httpResponse *HttpResponse
	var httpReponseError *HttpError
	if h.ctx == nil {
		httpResponse, httpReponseError = HttpGet(h, h.url, h.data, h.header, h.cookie, h.timeOut)
	} else {
		httpResponse, httpReponseError = HttpGetWithCtx(h.ctx, h, h.url, h.data, h.header, h.cookie, h.timeOut)
	}

	requestMsg := fmt.Sprintf("[info]发起请求-%s:[%s], method:[%s] data:[%+v], 当前次数:[%d],cooke信息:[%+v], 头信息:[%+v]", h.identifier, h.url, "GET", h.data, h.tryTime, h.cookie, h.header)
	log(requestMsg)
	if httpReponseError != nil {
		//请求超时
		switch httpReponseError.Code {
		case HTTP_ERROR_TIME_OUT: //请求超时
			if h.tryTime >= h.maxTryTime { //超过次数
				httpReponseError.TryTime = h.tryTime
				requestMsg := fmt.Sprintf("[error]请求超时-%s:[%s], 当前次数:[%d], 参数:[%+v], 头信息:[%+v]", h.identifier, h.url, h.tryTime, h.data, h.header)
				log(requestMsg)
				if h.errorFun != nil {
					h.errorFun(h.identifier, httpReponseError)
				}
				return
			} else {
				h.tryTime++
				h.HttpGetReTryTimeOut()
				return
			}
		default: //通用错误
			requestMsg := fmt.Sprintf("[error]请求错误-%s:[%s], 当前次数:[%d], 参数:[%+v], 头信息:[%+v], error:[%+v]", h.identifier, h.url, h.tryTime, h.data, h.header, httpReponseError)
			log(requestMsg)
			h.errorFun(h.identifier, httpReponseError)
			return
		}
	}
	httpResponse.TryTime = h.tryTime
	if h.successFun != nil {
		h.successFun(h.identifier, httpResponse)
	}
}

/*
*
发送post json
*/
func HttpPostJson(httpUrl string, data []byte, timeOut int) (*HttpResponse, *HttpError) {
	//"application/x-www-form-urlencoded"

	req, err := http.NewRequest("POST", httpUrl, bytes.NewReader(data))
	if err != nil {
		return nil, &HttpError{Code: HTTP_ERROR, Error: err}
	}

	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	//超时处理
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeOut)*time.Second)
	defer cancel()
	//isTimeOut := func() bool {
	//	select {
	//	default:
	//		return false
	//	case <-ctx.Done():
	//		// 是否超时
	//		return errors.Is(ctx.Err(), context.DeadlineExceeded)
	//	}
	//}
	req = req.WithContext(ctx)
	req.Close = true
	resp, ers := http.DefaultClient.Do(req)

	if ers != nil {
		if errors.Is(ers, context.DeadlineExceeded) {
			return nil, &HttpError{Code: HTTP_ERROR_TIME_OUT, Error: ers}
		}
		return nil, &HttpError{Code: HTTP_ERROR, Error: ers}
	}
	defer func(Body io.ReadCloser) {
		errd := Body.Close()
		if errd != nil {

		}
	}(resp.Body)
	result, errs := ioutil.ReadAll(resp.Body)
	if errs != nil {
		return nil, &HttpError{Code: HTTP_ERROR, Error: errs}
	}
	content := string(result)
	httpResponse := HttpResponse{Resp: resp, StatusCode: resp.StatusCode, Content: content}
	return &httpResponse, nil
}

/*
* 发送post请求
* @param httpUrl string 请求地址
* @param data interface 发送数据
* @param header map[string]string 请求头
* @param timeOut 超时时间(秒)
 */
func HttpPost(httpClient *httpClient, httpUrl string, data interface{}, header map[string]string, cookie []*http.Cookie, timeOut int) (*HttpResponse, *HttpError) {
	return httpSend(nil, httpClient, http.MethodPost, httpUrl, data, header, cookie, timeOut, httpClient.typeOutTime)
}

func HttpPostWithCtx(ctx context.Context, httpClient *httpClient, httpUrl string, data interface{}, header map[string]string, cookie []*http.Cookie, timeOut int) (*HttpResponse, *HttpError) {
	return httpSend(ctx, httpClient, http.MethodPost, httpUrl, data, header, cookie, timeOut, httpClient.typeOutTime)
}

/*
* 发送get请求
* @param httpUrl string 请求地址
* @param header map[string]string 请求头
* @param timeOut 超时时间(秒)
 */
func HttpGet(httpClient *httpClient, httpUrl string, data interface{}, header map[string]string, cookie []*http.Cookie, timeOut int) (*HttpResponse, *HttpError) {
	return httpSend(nil, httpClient, http.MethodGet, httpUrl, data, header, cookie, timeOut, httpClient.typeOutTime)
}

func HttpGetWithCtx(ctx context.Context, httpClient *httpClient, httpUrl string, data interface{}, header map[string]string, cookie []*http.Cookie, timeOut int) (*HttpResponse, *HttpError) {
	return httpSend(ctx, httpClient, http.MethodGet, httpUrl, data, header, cookie, timeOut, httpClient.typeOutTime)
}

func httpSend(ctx context.Context, httpClient *httpClient, method string, httpUrl string, data interface{}, header map[string]string, cookie []*http.Cookie, timeOut int, timeOutType TimeOutType) (*HttpResponse, *HttpError) {

	var gCurCookies []*http.Cookie
	gCurCookieJar, _ := cookiejar.New(nil)

	body := ""
	if method == http.MethodPost {
		contentType, ok := header["Content-Type"]
		if ok {
			if hasJson := strings.Index(contentType, "application/json"); hasJson == -1 {
				var dataMap map[string]interface{}
				bytesData, jsErr := json.Marshal(data)
				if jsErr != nil {
					return nil, &HttpError{Code: HTTP_ERROR, Error: jsErr}
				}
				err := json.Unmarshal(bytesData, &dataMap)
				if err != nil {
					return nil, &HttpError{Code: HTTP_ERROR, Error: err}
				}
				body = mapToUrlString(dataMap)
			} else {
				switch data.(type) {
				case string:
					body = data.(string)
				default:
					bytesData, jsErr := json.Marshal(data)
					if jsErr != nil {
						return nil, &HttpError{Code: HTTP_ERROR, Error: jsErr}
					}
					body = string(bytesData)
				}
				//bytesData, jsErr := json.Marshal(data)
				//if jsErr != nil {
				//	return nil, &HttpError{Code: HTTP_ERROR, Error: jsErr}
				//}
				//body = string(bytesData)
			}
		} else {
			bytesData, jsErr := json.Marshal(data)
			if jsErr != nil {
				return nil, &HttpError{Code: HTTP_ERROR, Error: jsErr}
			}
			body = string(bytesData)
		}
	}

	req, err := http.NewRequest(method, httpUrl, strings.NewReader(body))
	if err != nil {
		return nil, &HttpError{Code: HTTP_ERROR, Error: err}
	}

	if method == http.MethodGet && data != nil {
		getData := data.(map[string]string)
		q := req.URL.Query()
		for k, v := range getData {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	if header != nil {
		httpClient.headerReadLock.RLock()
		for k, v := range header {
			req.Header.Set(k, v)
		}
		httpClient.headerReadLock.RUnlock()
	}

	if cookie != nil && len(cookie) > 0 {
		for _, v := range cookie {
			req.AddCookie(v)
		}
	}

	//超时处理
	var timeOutDuration time.Duration
	switch timeOutType {
	case TIME_OUT_TYPE_SECOND:
		timeOutDuration = time.Duration(timeOut) * time.Second
	case TIME_OUT_TYPE_MILLISECOND:
		timeOutDuration = time.Duration(timeOut) * time.Millisecond
	default:
		timeOutDuration = time.Duration(timeOut) * time.Second
	}

	if ctx == nil {
		ctxx, cancel := context.WithTimeout(context.Background(), timeOutDuration)
		ctx = ctxx
		defer cancel()
	}

	//isTimeOut := func() bool {
	//	select {
	//	default:
	//		return false
	//	case <-ctx.Done():
	//		// 是否超时
	//		return errors.Is(ctx.Err(), context.DeadlineExceeded)
	//	}
	//}
	//fmt.Println(isTimeOut())
	transport := &http.Transport{
		//MaxIdleConns:        1000,
		IdleConnTimeout:     900 * time.Second,
		MaxIdleConnsPerHost: 1000,
	}
	httpClients := &http.Client{
		Transport: transport,
		Jar:       gCurCookieJar,
	}
	req = req.WithContext(ctx)
	req.Close = true
	req.Proto = "HTTP/1.1"
	resp, ers := httpClients.Do(req)

	if ers != nil {
		if errors.Is(ers, context.DeadlineExceeded) {
			return nil, &HttpError{Code: HTTP_ERROR_TIME_OUT, Error: ers}
		}
		return nil, &HttpError{Code: HTTP_ERROR_TIME_OUT, Error: ers}
	}
	defer func(Body io.ReadCloser) {
		errd := Body.Close()
		if errd != nil {

		}
	}(resp.Body)
	gCurCookies = gCurCookieJar.Cookies(req.URL)

	result, errs := ioutil.ReadAll(resp.Body)
	if errs != nil {
		return nil, &HttpError{Code: HTTP_ERROR, Error: errs}
	}
	content := string(result)
	httpResponse := HttpResponse{Resp: resp, StatusCode: resp.StatusCode, Cookie: gCurCookies, Header: resp.Header, Content: content, ContentBytes: result}
	return &httpResponse, nil
}

// 写入日志
func log(message string, args ...interface{}) {
	//fmt.Println(message)
	//Log2.LogCustomFile("request_client", message, args)
}

/*
*
通过雪花算法生成唯一id
*/
func getUniqueNum(nodeId int64) (string, error) {
	worker, err := NewWorker(nodeId)
	if err != nil {
		return "0", err
	}
	id := worker.GetId()
	idStr := strconv.FormatInt(id, 10)
	return idStr, nil
}

func mapToUrlString(dataMap map[string]interface{}) string {
	if dataMap == nil {
		return ""
	}
	param := url.Values{}
	for k, v := range dataMap {
		switch v.(type) {
		case int:
			param.Add(k, strconv.Itoa(v.(int)))
		case string:
			param.Add(k, v.(string))
		case float64:
			param.Add(k, fmt.Sprintf("%f", v.(float64)))
		}
	}
	return param.Encode()
}

func mapToStr(dataMap map[string]interface{}) map[string]string {
	mp := make(map[string]string)
	if dataMap == nil {
		return mp
	}
	for k, v := range dataMap {
		switch v.(type) {
		case int:
			mp[k] = strconv.Itoa(v.(int))
		case string:
			mp[k] = v.(string)
		case float64:
			mp[k] = fmt.Sprintf("%f", v.(float64))
		}
	}
	return mp
}
